package fr.univlille.iutinfo.m3105.modelQ1;

public class Thermogeekostat implements ITemperature {
	private double temperature;
	@Override
	public void setTemperature(double d) {
		// TODO Auto-generated method stub
		this.temperature=d;
	}

	@Override
	public Double getTemperature() {
		// TODO Auto-generated method stub
		return this.temperature;
	}

	@Override
	public void incrementTemperature() {
		// TODO Auto-generated method stub
		this.setTemperature(this.getTemperature()+1);
	}

	@Override
	public void decrementTemperature() {
		// TODO Auto-generated method stub
		this.setTemperature(this.getTemperature()-1);
	}

}
