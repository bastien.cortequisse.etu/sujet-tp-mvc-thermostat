package fr.univlille.iutinfo.m3105;

import fr.univlille.iutinfo.m3105.modelQ1.Thermogeekostat;
import fr.univlille.iutinfo.m3105.viewQ1.TextView;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

public class Main extends Application {

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		Thermogeekostat thermo = new Thermogeekostat();
		thermo.setTemperature(0);
		TextView txt=new TextView(thermo);
		
		Scene scene = new Scene(new HBox(txt.getBoite()));
		primaryStage.setTitle("Super Thermostat");
		primaryStage.setScene(scene);
		primaryStage.show();
	}

}
