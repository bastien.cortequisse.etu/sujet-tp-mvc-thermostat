package fr.univlille.iutinfo.m3105.viewQ1;

import fr.univlille.iutinfo.m3105.modelQ1.Thermogeekostat;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

public class TextView extends Stage implements ITemperatureView {
	private Thermogeekostat model;
	private Label l;
	private Button plus, moins;
	private HBox boite;
	
	public TextView(Thermogeekostat model) {
		// TODO Auto-generated constructor stub
		this.model=model;
		this.l=new Label(""+this.model.getTemperature());
		
		this.plus=new Button("+");
		plus.setOnAction(e ->{
			this.incrementAction();
		});
		
		this.moins=new Button("-");
		moins.setOnAction(e ->{
			this.decrementAction();
		});
		
		this.boite=new HBox(this.moins,this.l,this.plus);
		
	}

	@Override
	public double getDisplayedValue() {
		// TODO Auto-generated method stub
		return this.model.getTemperature();
	}

	@Override
	public void incrementAction() {
		// TODO Auto-generated method stub
		this.model.incrementTemperature();
		this.l.setText(""+this.getDisplayedValue());
	}

	@Override
	public void decrementAction() {
		// TODO Auto-generated method stub
		this.model.decrementTemperature();
		this.l.setText(""+this.getDisplayedValue());
	}

	public HBox getBoite() {
		return this.boite;
	}

	
}
